package ictgradschool.industry.collections;

import ictgradschool.Keyboard;

import java.util.*;

public class ex01
    {
    public void start()
        {
        ArrayList<String> list = new ArrayList<String>();
        String[] array = {"ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN"};
        Collections.addAll(list, array);

        for (String ele : list) {
            System.out.print(ele + " ");
        }
        System.out.println();
        outerloop:
        while (true) {
            int choose = Integer.parseInt(Keyboard.readInput());
            switch (choose) {
                case 1:
                    for (int i = 0; i < list.size(); i++) {
                        String str = list.get(i);
                        str = str.toLowerCase();
                        list.remove(i);
                        list.add(i, str);
                    }
                    break;

                case 2:
                    ListIterator<String> myIterator = list.listIterator();
                    while (myIterator.hasNext()) {
                        String str = myIterator.next();
                        str = str.toLowerCase();
                        myIterator.set(str);
                    }
                    break;

                case 3:
                    ArrayList<String> list2 = new ArrayList<>();
                    for (String ele : list) {
                        ele = ele.toLowerCase();
                        list2.add(ele);
                    }
                    list.clear();
                    list.addAll(list2);
                    break;
                case 4:
                    break outerloop;
            }

            for (String ele : list) {
                System.out.print(ele + " ");
            }
            System.out.println();
        }
        }

    public static void main(String[] args)
        {
        new ex01().start();
        }
    }
